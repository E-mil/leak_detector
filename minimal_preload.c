#include <unistd.h>

__attribute__ ((constructor))
static void at_start() {
    write(STDOUT_FILENO, "...\n", 4);
}
