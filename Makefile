
all:
	$(CC) -fPIC -Wall -shared -ldl -o leak_detector.so leak_detector.c
	$(CC) -Wall -Werror -g -fno-omit-frame-pointer -Wl,--export-dynamic -o test test.c -ldl
