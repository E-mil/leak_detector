/**
 * Conservative memory leak detector. (only detects unreachable memory)
 * Wraps calls to malloc & friends.
 * Keeps track of the allocations in a doubly linked list stored in headers.
 * Performs garbage detection using a mark and sweep garbage collection algorithm.
 * Prints stack traces for allocations of memory that was then leaked.
 *
 * No guarantees of thread safety.
 *
 * Requirements:
 * glibc 2.0 or later, for dladdr()
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE // Needed according to documentation online
#endif // _GNU_SOURCE
#ifndef __USE_GNU
#define __USE_GNU // Needed according to my dlfcn.h file
#endif // __USE_GNU

#define NBR_STACK_TRACES 5

#include <dlfcn.h>
#include <link.h>
#include <elf.h>

//extern char __data_start, _end;
extern void __libc_freeres(void);

typedef struct {
    void *previous_allocation;
    void *next_allocation;
    uint64_t stack_trace[NBR_STACK_TRACES];
    uint32_t check;
    uint32_t size; // msb of this is indicating whether this allocation has been marked
    //uint64_t stack_trace[5];
} malloc_wrapper_header;

typedef struct data_segment {
    struct data_segment *next_segment;
    void *start_address;
    void *end_address;
} data_segment;

static data_segment *data_segments_head = NULL, *data_segments_tail = NULL;
static char finding_data_segments = 0;

static uint64_t *start_of_heap; // used for storing the last allocation

static void print_allocation_list() {
    uint64_t crt_alloc = *start_of_heap;
    while (crt_alloc != 0) {
        printf("%ld\n", crt_alloc);
        crt_alloc = *((uint64_t *)crt_alloc);
    }
}

static void print_marked_allocations() { // print marked allocations in the list
    printf("Marked allocations:\n");
    uint64_t crt_alloc = *start_of_heap;
    while (crt_alloc != 0) {
        if ((*((uint32_t *)(crt_alloc + sizeof(malloc_wrapper_header) - 4)) & 0b10000000000000000000000000000000) > 0) {
            printf("%ld\n", crt_alloc + sizeof(malloc_wrapper_header));
        }
        crt_alloc = *((uint64_t *)crt_alloc);
    }
}

static void print_stacktrace(malloc_wrapper_header *allocation) {
    int i, ret;
    void *func_addr;
    Dl_info func_info;

    printf("--------------------------\n");
    printf("Stack trace for: %ld\n", (uint64_t)allocation + sizeof(malloc_wrapper_header));

    for (i = 0; i < NBR_STACK_TRACES; i++) {
        func_addr = (void *)allocation->stack_trace[i];
        //printf("func_addr: %p\n", func_addr);
        if (func_addr == 0) {
            break;
        }
        ret = dladdr((void *)func_addr, &func_info);
        //printf("hello\n");

        if (ret != 0) {
            printf("fbase: %p, saddr: %p\n", func_info.dli_fbase, func_info.dli_saddr);
            if (func_info.dli_saddr != NULL) {
                printf("%s\n", func_info.dli_sname);
            }
            //printf("fname: %s, sname: %s\n", func_info.dli_fname, func_info.dli_sname);
        } else {
            printf("dladdr failed\n");
        }
    }
    printf("--------------------------\n");
}

static void print_unmarked_allocations() { // print non marked allocations in the list
    printf("Unmarked allocations:\n");
    uint64_t crt_alloc = *start_of_heap;
    while (crt_alloc != 0) {
        if ((*((uint32_t *)(crt_alloc + sizeof(malloc_wrapper_header) - 4)) & 0b10000000000000000000000000000000) == 0) {
            //printf("%ld\n", crt_alloc + sizeof(malloc_wrapper_header));
            print_stacktrace((void *)crt_alloc);
        }
        crt_alloc = *((uint64_t *)crt_alloc);
    }
}

static void check_heap(uint64_t heap_pos, uint64_t *end_of_heap) {
    malloc_wrapper_header *header = (void *)(heap_pos - sizeof(malloc_wrapper_header));

    //printf("marked: %ld, heap_pos: %ld\n", (uint64_t)header, heap_pos);

    uint32_t size;
    //write(STDOUT_FILENO, "???\n", 4);
    if (header->check == 0b10101010101010101010101010101010 && (header->size & 0b10000000000000000000000000000000) == 0) {
        //write(STDOUT_FILENO, "MARK\n", 5);
        size = header->size;
        header->size |= 0b10000000000000000000000000000000;
        uint32_t i;
        for (i = 0; i < size; i++) { // loop through allocated memory and check for pointers recursively
            if (*((uint64_t *)heap_pos) > (uint64_t)start_of_heap && *((uint64_t *)heap_pos) < (uint64_t)end_of_heap) {
                check_heap(*((uint64_t *)heap_pos), end_of_heap);
            }
            heap_pos += 8;
        }
    }
    //write(STDOUT_FILENO, "rip?\n", 5);
}

static void mark_accessible_memory() {
    uint64_t *end_of_heap = sbrk(0);
    uint64_t rsp, rbp;
    asm("movq %%rsp, %0" : "=r"(rsp)); // get %rsp
    asm("movq %%rbp, %0" : "=r"(rbp)); // get %rbp

    uint64_t pos = rsp, bottom = rbp;
    while (pos < bottom) { // loop through stack
        //printf("%ld\n", *((uint64_t*)pos));

        //check if pos points to allocated memory
        if (*((uint64_t *)pos) > (uint64_t)start_of_heap && *((uint64_t *)pos) < (uint64_t)end_of_heap) {
            //printf("mark: %ld\n", *((uint64_t *)pos));
            check_heap(*((uint64_t *)pos), end_of_heap);
        }

        //printf("*pos: %ld\n", *((uint64_t *)pos));

        pos += 8;
        if (pos == bottom) {
            bottom = *((uint64_t*)bottom);
            pos += 8;
        }
        //printf("pos: %ld, bottom: %ld\n", pos, bottom);
    }

    uint64_t *i;
    data_segment *crt_data_segment = data_segments_head;
    while (crt_data_segment != NULL) {
        // loop through initialized data and bss for the executable and all the shared libs
        for (i = (uint64_t *)crt_data_segment->start_address; i < (uint64_t *)crt_data_segment->end_address; i++) {
            if (*i > (uint64_t)start_of_heap && *i < (uint64_t)end_of_heap) {
                check_heap(*i, end_of_heap);
            }
        }
        crt_data_segment = crt_data_segment->next_segment;
    }

    /*uint64_t *i;
    for (i = (uint64_t *)&__data_start; i < (uint64_t *)&_end; i++) { // loop through initialized data and bss
        if (*i > (uint64_t)start_of_heap && *i < (uint64_t)end_of_heap) {
            check_heap(*i, end_of_heap);
        }
    }*/
}

// loop through the stack and all allocations in the heap
// mark all ponters that successfully get marked
static void check_for_memory_leaks() {
    //write(STDOUT_FILENO, "p\n", 2);
    mark_accessible_memory();
    //write(STDOUT_FILENO, "q\n", 2);
    print_marked_allocations();
    //write(STDOUT_FILENO, "w\n", 2);
    print_unmarked_allocations();
    //write(STDOUT_FILENO, "e\n", 2);

    //TODO: SET MARKED TO 0 FOR LIST OF ALLOCATIONS

    /*printf("end: %ld\n", (uint64_t)&_end);
    printf("data_start: %ld\n", (uint64_t)&__data_start);
    uint64_t *i;
    printf("data_start to end:\n");
    for (i = (uint64_t *)&__data_start; i < (uint64_t *)&_end; i++) {
        printf("data: %ld\n", *((uint64_t *)i));
    }*/
}

static void set_stack_trace(malloc_wrapper_header *header) {
    uint64_t rsp, rbp;
    asm("movq %%rsp, %0" : "=r"(rsp)); // get %rsp
    asm("movq %%rbp, %0" : "=r"(rbp)); // get %rbp

    //write(STDOUT_FILENO, "sst1\n", 5);

    uint64_t func_addr;

    uint64_t new_rbp = rbp;
    char i = 0;
    //write(STDOUT_FILENO, "sst2\n", 5);
    do {
        //write(STDOUT_FILENO, "sst4\n", 5);
        func_addr = *((uint64_t *)(new_rbp + 8));
        header->stack_trace[i] = func_addr;
        i++;
        //write(STDOUT_FILENO, "sst6\n", 5);
        rbp = new_rbp;
        new_rbp = *((uint64_t *)(new_rbp));
        //write(STDOUT_FILENO, "sst5\n", 5);
    } while (new_rbp > rbp && i < 5);
    //write(STDOUT_FILENO, "sst3\n", 5);
    if (i < 5) {
        header->stack_trace[i] = 0;
    }
}

static void set_malloc_wrapper_header(void *allocated_mem, uint32_t size) {
    //write(STDOUT_FILENO, "mw1\n", 4);
    malloc_wrapper_header *header = allocated_mem;
    //write(STDOUT_FILENO, "mw6\n", 4);
    //write(STDOUT_FILENO, "mw7\n", 4);
    header->previous_allocation = (void *)*start_of_heap; // point to previous allocation
    //write(STDOUT_FILENO, "mw5\n", 4);
    if (*start_of_heap != 0) {
        *((uint64_t *)(*start_of_heap + 8)) = (uint64_t)allocated_mem; // make previous allocation point to this one
    }
    //write(STDOUT_FILENO, "mw2\n", 4);
    header->next_allocation = 0; // Set pointer to 0 for now
    header->check = 0b10101010101010101010101010101010; // for checking whether this is an allocation, not waterproof
    header->size = size; // if size is greater than 2^31 - 1 there will be issues
    //write(STDOUT_FILENO, "mw3\n", 4);
    set_stack_trace(header);
    //write(STDOUT_FILENO, "mw4\n", 4);
    *start_of_heap = (uint64_t)allocated_mem;
}


typedef void *(*malloc_func_type)(size_t size);
//typedef void *(*calloc_func_type)(size_t nitems, size_t size);
typedef void *(*realloc_func_type)(void *ptr, size_t size);
typedef void (*free_func_type)(void *ptr);

void *valloc(size_t size) {
    static malloc_func_type original_valloc = 0;
    if (original_valloc == 0) {
        original_valloc = (malloc_func_type)dlsym(RTLD_NEXT, "valloc");
    }
    write(STDOUT_FILENO, "valloc\n", 7);
    if (size == 0) {
        return NULL;
    }
    size_t new_size = size + sizeof(malloc_wrapper_header);
    void *allocated_mem = original_valloc(new_size);
    set_malloc_wrapper_header(allocated_mem, size);
    return allocated_mem + sizeof(malloc_wrapper_header);
}

void *malloc(size_t size) {
    static malloc_func_type original_malloc = 0;
    if (original_malloc == 0) {
        original_malloc = (malloc_func_type)dlsym(RTLD_NEXT, "malloc");
    }
    //return original_malloc(size);
    write(STDOUT_FILENO, "malloc\n", 7);
    if (size == 0) {
        return NULL;
    }
    if (finding_data_segments) {
        return original_malloc(size);
    }
    size_t new_size = size + sizeof(malloc_wrapper_header);
    void *allocated_mem = original_malloc(new_size);
    //write(STDOUT_FILENO, "malloc1\n", 8);
    set_malloc_wrapper_header(allocated_mem, size);
    //write(STDOUT_FILENO, "malloc2\n", 8);
    return allocated_mem + sizeof(malloc_wrapper_header);
}

void *calloc(size_t nitems, size_t size) {
    /*static calloc_func_type original_calloc = 0;
    if (original_calloc == 0) {
        original_calloc = (calloc_func_type)dlsym(RTLD_NEXT, "calloc");
    }*/
    write(STDOUT_FILENO, "calloc\n", 7);
    size_t total_size = size * nitems;
    char *ptr = malloc(total_size);
    if (ptr == NULL) {
        return NULL;
    }
    memset(ptr + sizeof(malloc_wrapper_header), 0, total_size);
    return (void *)ptr + sizeof(malloc_wrapper_header);//original_calloc(nitems, size);
}

void free(void *ptr);
void print_stack();

void *realloc(void *ptr, size_t size) {
    static realloc_func_type original_realloc = 0;
    if (original_realloc == 0) {
        original_realloc = (realloc_func_type)dlsym(RTLD_NEXT, "realloc");
    }
    write(STDOUT_FILENO, "realloc\n", 8);
    if (!ptr) {
        write(STDOUT_FILENO, "mlc\n", 4);
        return malloc(size);
    }
    if (size == 0) {
        write(STDOUT_FILENO, "fre\n", 4);
        free(ptr);
        return NULL;
    }
    //print_allocation_list();
    //print_stack();
    //printf("ptr: %ld - mwh= %ld\n", (uint64_t)ptr, (uint64_t)ptr - sizeof(malloc_wrapper_header));
    malloc_wrapper_header *realloc_ptr = original_realloc(ptr - sizeof(malloc_wrapper_header), size + sizeof(malloc_wrapper_header));
    //printf("realloc_ptr: %ld\n", (uint64_t)realloc_ptr);
    write(STDOUT_FILENO, "done\n", 5);
    if (!realloc_ptr) {
        return NULL;
    }
    realloc_ptr->size = size;
    if (realloc_ptr->next_allocation) {
        ((malloc_wrapper_header *)(realloc_ptr->next_allocation))->previous_allocation = (void *)realloc_ptr;
    }
    if (realloc_ptr->previous_allocation) {
        ((malloc_wrapper_header *)(realloc_ptr->previous_allocation))->next_allocation = (void *)realloc_ptr;
    }
    //printf("ret rlc: %ld\n", );
    return (char *)realloc_ptr + sizeof(malloc_wrapper_header);
}

void print_stack() {
    uint64_t rsp, rbp;
    asm("movq %%rsp, %0" : "=r"(rsp)); // get %rsp
    asm("movq %%rbp, %0" : "=r"(rbp)); // get %rbp

    uint64_t func_addr;
    int ret;
    Dl_info func_info;

    uint64_t new_rbp = rbp;
    do {
        func_addr = *((uint64_t *)(new_rbp + 8));
        printf("rbp: %ld, func_addr: %ld\n", new_rbp, func_addr);
        ret = dladdr((void *)func_addr, &func_info);

        if (ret != 0) {
            printf("fname: %s, sname: %s\n", func_info.dli_fname, func_info.dli_sname);
        } else {
            printf("dladdr failed\n");
        }

        rbp = new_rbp;
        new_rbp = *((uint64_t *)(new_rbp));
    } while (new_rbp > rbp);
}

void free(void *ptr) {
    static free_func_type original_free = 0;
    //print_allocation_list();
    if (original_free == 0) {
        original_free = (free_func_type)dlsym(RTLD_NEXT, "free");
    }
    //return original_free(ptr); //remove

    if (ptr == 0) { // Do nothing when ptr is null
        return;
    }
    //write(STDOUT_FILENO, "free\n", 5);
    //print_stack();

    malloc_wrapper_header *current_header, *previous_header, *next_header;
    //write(STDOUT_FILENO, "0!\n", 3);
    current_header = (malloc_wrapper_header *)(ptr - sizeof(malloc_wrapper_header));
    //write(STDOUT_FILENO, "1!\n", 3);
    //printf("%ld\n", (uint64_t)ptr);
    if (current_header->check != 0b10101010101010101010101010101010) { // if not in allocated list, not waterproof
        write(STDOUT_FILENO, "WA!\n", 4);
        //printf("check: %d, size: %d, prev: %ld\n", current_header->check, current_header->size, (uint64_t)current_header->previous_allocation);
        return original_free(ptr);
    }
    uint64_t *prev_alloc, *next_alloc;
    prev_alloc = current_header->previous_allocation;
    next_alloc = current_header->next_allocation;
    //printf("crt: %ld, next: %ld, prev: %ld\n", (uint64_t)current_header, (uint64_t)next_alloc, (uint64_t)prev_alloc);
    if (prev_alloc != 0) { // if there is a previously allocated memory block still not freed
        previous_header = (malloc_wrapper_header *)prev_alloc;

        // make previous header point to next header
        previous_header->next_allocation = current_header->next_allocation;
        if (next_alloc == 0) {
            // if current allocation is last allocation make previous allocation last allocation
            *start_of_heap = (uint64_t)previous_header;
        }
    } else {
        if (next_alloc == 0) {
            *start_of_heap = 0;
        }
    }
    if (next_alloc != 0) { // if there is a more recent memory allocation still not freed
        next_header = (malloc_wrapper_header *)next_alloc;
        // make next header point to previous header
        next_header->previous_allocation = current_header->previous_allocation;
    }
    //print_stacktrace();
    //print_allocation_list();
    //check_for_memory_leaks();
    //printf("before\n");
    //print_allocation_list();
    //printf("ptr: %ld, crt_header: %ld\n", (uint64_t)ptr, (uint64_t)current_header);
    //write(STDOUT_FILENO, "...\n", 4);
    return original_free((void *)current_header); // NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
}

static int find_data_segments(struct dl_phdr_info *info, size_t size, void *data) {
    int i;
    for (i = 0; i < info->dlpi_phnum; i++) {
        if (info->dlpi_phdr[i].p_type == PT_LOAD) {
            /*printf("__data_start: %ld\n", (uint64_t)&__data_start);
            printf("_end: %ld\n", (uint64_t)&_end);
            printf("address of %s: %ld\n", info->dlpi_name, (uint64_t)(info->dlpi_addr + info->dlpi_phdr[i].p_vaddr));
            printf("diff: %ld\n", (uint64_t)&__data_start - info->dlpi_addr);
            printf("p_vaddr: %ld\n", info->dlpi_phdr[i].p_vaddr);*/
            if ((info->dlpi_phdr[i].p_flags & PF_W) != 0 && (info->dlpi_phdr[i].p_flags & PF_R) != 0) {
                //printf("name: %s\n", info->dlpi_name);
                //printf("RW\n");
                if (data_segments_tail == NULL) {
                    data_segments_head = malloc(sizeof(data_segment));
                    data_segments_tail = data_segments_head;
                } else {
                    data_segments_tail->next_segment = malloc(sizeof(data_segment));
                    data_segments_tail = data_segments_tail->next_segment;
                }
                data_segments_tail->next_segment = NULL;
                data_segments_tail->start_address = (void *)info->dlpi_phdr[i].p_vaddr + info->dlpi_addr;
                data_segments_tail->end_address = (void *)info->dlpi_phdr[i].p_memsz + info->dlpi_phdr[i].p_vaddr + info->dlpi_addr;
            }
            //printf("memsz: %ld, end_addr: %ld\n", info->dlpi_phdr[i].p_memsz, info->dlpi_phdr[i].p_memsz + info->dlpi_phdr[i].p_vaddr + info->dlpi_addr);
            /*Elf64_Dyn *dynamic_section = (Elf64_Dyn *)(info->dlpi_addr + info->dlpi_phdr[i].p_vaddr);
            while (dynamic_section->d_tag != DT_NULL) {
                if (dynamic_section->d_tag == DT_SYMTAB) {
                    printf("SYMTAB!\n");
                    Elf64_Sym *symbol_table = (Elf64_Sym *)dynamic_section->d_un.d_ptr;
                    printf("name: %d\n", symbol_table->st_name);
                }
                dynamic_section++;
            }*/
            /*Elf64_Ehdr *elfheader = (Elf64_Ehdr *)(info->dlpi_addr + info->dlpi_phdr[i].p_vaddr);
            printf("e_shoff: %ld\n", elfheader->e_shoff);
            printf("e_shnum: %d\n", elfheader->e_shnum);
            int j;
            Elf64_Shdr *section_header;
            for (j = 0; j < elfheader->e_shnum; j++) {
                section_header = (Elf64_Shdr *)((uint64_t)elfheader + elfheader->e_shoff + j * elfheader->e_shentsize);
                if (section_header->sh_type == SHT_PROGBITS) {
                    printf("SHT_PROGBITS!\n");
                }
                //printf("section_header: %p\n", section_header);
            }*/
        }
    }
    return 0;
}

static void at_start(void) __attribute__ ((constructor));
static void at_start() {
    start_of_heap = sbrk(8); // get starting address of heap and allocate space for one pointer
    mallopt(M_MMAP_MAX, 0); // disable mmap calls by malloc
    if (start_of_heap == (void *)-1) {
        write(STDOUT_FILENO, "sbrk failed\n", 12);
    }
    *start_of_heap = 0; // set value of first 8 bytes of heap to 0

    write(STDOUT_FILENO, "wat\n", 4);

    /*finding_data_segments = 1;
    dl_iterate_phdr(find_data_segments, NULL);
    finding_data_segments = 0;

    //printf("__data_start: %p, _end: %p\n", &__data_start, &_end);
    data_segment *crt_data_segment = data_segments_head; // remove
    while (crt_data_segment != NULL) {
        printf("start_address: %p, end_address: %p\n", crt_data_segment->start_address, crt_data_segment->end_address);
        printf("size: %ld\n", (uint64_t)crt_data_segment->end_address - (uint64_t)crt_data_segment->start_address);
        crt_data_segment = crt_data_segment->next_segment;
    }
    printf("constructor done\n");*/
}

static void cleanup(void) __attribute__ ((destructor));
static void cleanup() {
    //printf("HELLO\n");
    __libc_freeres();
    check_for_memory_leaks();
}

/*typedef void (*exit_func_type)(int status);

void exit(int status) {
    static exit_func_type original_exit = 0;
    if (original_exit == 0) {
        original_exit = (exit_func_type)dlsym(RTLD_NEXT, "exit");
    }
    printf("EXITING\n");
    original_exit(status);
}*/

/////// X-allocation functions ///////

/*void *xmalloc(size_t size) {
    write(STDERR_FILENO, "")
    void *ptr = malloc(size);
    if (!ptr) {
        write(STDERR_FILENO, "xmalloc failed\n", 15);
        exit(-1);
    }
    return ptr;
}

void *xcalloc(size_t nmemb, size_t size) {
    void *ptr = calloc(nmemb, size);
    if (!ptr) {
        write(STDERR_FILENO, "xcalloc failed\n", 15);
        exit(-1);
    }
    return ptr;
}

void *xrealloc(void *ptr, size_t size) {
    void *realloc_ptr = realloc(ptr, size);
    if (!realloc_ptr) {
        write(STDERR_FILENO, "xrealloc failed\n", 16);
        exit(-1);
    }
    return realloc_ptr;
}

void xfree(void *ptr) {
    free(ptr);
}*/
