#include <stdint.h>
#include <unistd.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE // Needed according to documentation online
#endif // _GNU_SOURCE
#ifndef __USE_GNU
#define __USE_GNU // Needed according to my dlfcn.h file
#endif // __USE_GNU

#include <dlfcn.h>

typedef void *(*malloc_func_type)(size_t size);
typedef void *(*calloc_func_type)(size_t nitems, size_t size);
typedef void *(*realloc_func_type)(void *ptr, size_t size);
typedef void (*free_func_type)(void *ptr);

/*void *malloc(size_t size) {
    static malloc_func_type original_malloc = 0;
    if (original_malloc == 0) {
        original_malloc = (malloc_func_type)dlsym(RTLD_NEXT, "malloc");
    }
    write(STDOUT_FILENO, "malloc\n", 7);
    return original_malloc(size);
}*/

/*void *calloc(size_t nitems, size_t size) {
    static calloc_func_type original_calloc = 0;
    if (original_calloc == 0) {
        original_calloc = (calloc_func_type)dlsym(RTLD_NEXT, "calloc");
    }
    write(STDOUT_FILENO, "calloc\n", 7);
    return original_calloc(nitems, size);
}

void *realloc(void *ptr, size_t size) {
    static realloc_func_type original_realloc = 0;
    if (original_realloc == 0) {
        original_realloc = (realloc_func_type)dlsym(RTLD_NEXT, "realloc");
    }
    write(STDOUT_FILENO, "realloc\n", 8);
    return original_realloc(ptr, size);
}*/

void free(void *ptr) {
    static free_func_type original_free = 0;
    if (original_free == 0) {
        original_free = (free_func_type)dlsym(RTLD_NEXT, "free");
    }
    write(STDOUT_FILENO, "free\n", 5);
    return original_free(ptr);
}