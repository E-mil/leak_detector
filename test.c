#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

extern void __libc_freeres(void);

char *hmm = (char *)15;
char *hmm2;
char *a;
char *b;
uint64_t hello = 400;

int main() {
    //printf("hmm: %ld\n", (uint64_t)&hmm);
    //printf("hmm2: %ld\n", (uint64_t)&hmm);
    hmm2 = malloc(12);
    printf("hmm2: %ld\n", (uint64_t)hmm2);
    //hmm = malloc(10);
    char *s = malloc(8);
    printf("old s: %ld\n", (uint64_t)s);
    //free(s);
    //printf("\n");
    char *tmp = malloc(12);
    printf("tmp: %ld\n", (uint64_t)tmp);
    //tmp = malloc(10);
    tmp[0] = 'a';
    s = malloc(9);
    printf("new s: %ld\n", (uint64_t)s);
    //free(tmp);
    //printf("\n");
    s[0] = 'a';
    s[1] = 'b';
    s[2] = 'c';
    s[3] = '\0';
    //printf("%s\n", s);
    //printf(":)\n");
    //printf("%ld\n", hello);
    //printf("s: %ld, tmp: %ld\n", (uint64_t)s, (uint64_t)tmp);
    //__libc_freeres(); // free libc memory

    free(s);
    free(tmp);
    free(hmm2);
    printf("hello :)\n");
}
