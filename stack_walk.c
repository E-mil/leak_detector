#include <stdio.h>
#include <stdint.h>

#define __USE_GNU
#include <dlfcn.h>

void print_stack() {
    uint64_t rsp, rbp;
    asm("movq %%rsp, %0" : "=r"(rsp)); // get %rsp
    asm("movq %%rbp, %0" : "=r"(rbp)); // get %rbp

    uint64_t func_addr;
    int ret;
    Dl_info func_info;

    uint64_t new_rbp = rbp;
    do {
        func_addr = *((uint64_t *)(new_rbp + 8));
        printf("rbp: %ld, func_addr: %ld\n", new_rbp, func_addr);
        ret = dladdr((void *)func_addr, &func_info);

        if (ret != 0) {
            printf("fname: %s, sname: %s\n", func_info.dli_fname, func_info.dli_sname);
        } else {
            printf("dladdr failed\n");
        }

        rbp = new_rbp;
        new_rbp = *((uint64_t *)(new_rbp));
    } while (new_rbp > rbp);

/*    func_addr = *((uint64_t *)(rbp + 8));
    printf("rsp: %ld, rbp: %ld, func_addr: %ld\n", rsp, rbp, func_addr);
    ret = dladdr((void *)func_addr, &func_info);

    if (ret != 0) {
        printf("fname: %s, sname: %s\n", func_info.dli_fname, func_info.dli_sname);
    } else {
        printf("dladdr failed\n");
    }

    rbp = *((uint64_t *)rbp);
    func_addr = *((uint64_t *)(rbp + 8));
    printf("rbp: %ld, func_addr: %ld\n", rbp, func_addr);

    ret = dladdr((void *)func_addr, &func_info);

    if (ret != 0) {
        printf("fname: %s, sname: %s\n", func_info.dli_fname, func_info.dli_sname);
    } else {
        printf("dladdr failed\n");
    }

    rbp = *((uint64_t *)rbp);

    printf("rbp: %ld\n", rbp);*/

    /*uint64_t pos = rsp, bottom = rbp;
    while (pos < bottom) {
        printf("%ld\n", *((uint64_t*)pos));
        pos += 8;
        if (pos == bottom) {
            bottom = *((uint64_t*)bottom);
            pos += 16;
        }
        //printf("pos: %ld, bottom: %ld\n", pos, bottom);
    }*/
    //printf("%ld\n", *((uint64_t*)pos));

    //printf("%ld, %ld, %ld, %ld\n", rsp, rbp, *((uint64_t*)rbp), *((uint64_t*)*((uint64_t*)rbp)));
}

void a() {
    print_stack();
}

void b() {
    a();
}

void c() {
    b();
}

int main() {
    c();
}
